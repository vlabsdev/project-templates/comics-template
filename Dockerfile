# Dockerfile to build and run the module on the cloud

######## Stage 1 pdf conversion
FROM alpine:latest as converter
RUN apk update \
    && apk add lighttpd \
    && apk add --upgrade poppler-utils \
    && rm -rf /var/cache/apk/*
WORKDIR /var/www/localhost/htdocs
COPY activity/*pdf /var/www/localhost/htdocs

RUN pdftoppm -r 75 *pdf page -png
RUN rm *pdf
RUN ls -v | cat -n | while read n f; do mv -n "$f" "$n.png"; done
RUN mkdir convert
RUN mv /var/www/localhost/htdocs/*.png /var/www/localhost/htdocs/convert/
RUN ls convert/ | wc -l > totalpage.txt


######## Stage 2 python build
FROM registry.gitlab.com/vlabsdev/core-applications/jigyasa-sites/jigyasa-lab-templates/template-combined-cdn-links/main:latest AS builder
ARG PROJECT_NAME
ARG NODAL_NAME
ARG WGET_TOKEN
ARG CDN_LINK
COPY ./manual /app/manual/
COPY details.json /app
COPY --from=converter /var/www/localhost/htdocs/totalpage.txt /app
RUN ls -ltr
# integration by python
RUN python3 script.py
RUN ls -ltr
RUN ls -ltr activity/
RUN rm *.py
RUN rm *.txt


######## Stage 3 s3 push
FROM d3fk/s3cmd AS s3-bucket
ARG S3_AKEY
ARG S3_SKEY
ARG PROJECT_NAME
ARG NODAL_NAME
ARG S3_LAB_MANUAL
ARG S3_ACTIVITY
ARG S3_ACL
ARG S3_HOST
ARG CDN_LINK

WORKDIR /home

# Pushing BUILD CONTEXT to CDN
RUN mkdir -p /home/$NODAL_NAME/$PROJECT_NAME
COPY --from=builder /app /home/$NODAL_NAME/$PROJECT_NAME
RUN ls -ltr /home/$NODAL_NAME/$PROJECT_NAME/
RUN ls -ltr 
RUN s3cmd put --recursive /home/ $S3_LAB_MANUAL/ --host-bucket=bucket --host=$S3_HOST --no-check-certificate --access_key=$S3_AKEY --secret_key=$S3_SKEY

# Pushing ACTIVITY to CDN
RUN mkdir -p activity/$NODAL_NAME/$PROJECT_NAME
COPY --from=converter /var/www/localhost/htdocs/convert /home/activity/$NODAL_NAME/$PROJECT_NAME
RUN ls -ltr /home/activity/$NODAL_NAME/$PROJECT_NAME
RUN s3cmd put --recursive /home/activity/ $S3_ACTIVITY/ --host-bucket=bucket --host=$S3_HOST --no-check-certificate --access_key=$S3_AKEY --secret_key=$S3_SKEY
RUN s3cmd setacl $S3_ACL --recursive $S3_ACTIVITY/$NODAL_NAME/$PROJECT_NAME/ --host-bucket=bucket --host=$S3_HOST --no-check-certificate --access_key=$S3_AKEY --secret_key=$S3_SKEY 

